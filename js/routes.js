angular.module('app.routes', [])

.config(function($stateProvider, $urlRouterProvider) {

  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider
    

      .state('tabsController', {
    url: '/page1',
    templateUrl: 'templates/tabsController.html',
    abstract:true
  })

  .state('tabsController.lista', {
    url: '/page6',
    views: {
      'tab5': {
        templateUrl: 'templates/lista.html',
        controller: 'listaCtrl'
      }
    }
  })

  .state('tabsController.mapa', {
    url: '/page7',
    views: {
      'tab6': {
        templateUrl: 'templates/mapa.html',
        controller: 'mapaCtrl'
      }
    }
  })

  .state('tabsController.escanear', {
    url: '/page8',
    views: {
      'tab7': {
        templateUrl: 'templates/escanear.html',
        controller: 'escanearCtrl'
      }
    }
  })

  .state('tabsController.carrito', {
    url: '/page9',
    views: {
      'tab8': {
        templateUrl: 'templates/carrito.html',
        controller: 'carritoCtrl'
      }
    }
  })

  .state('tabsController.ajustes', {
    url: '/page10',
    views: {
      'tab9': {
        templateUrl: 'templates/ajustes.html',
        controller: 'ajustesCtrl'
      }
    }
  })

  .state('iniciarSesiN', {
    url: '/page11',
    templateUrl: 'templates/iniciarSesiN.html',
    controller: 'iniciarSesiNCtrl'
  })

  .state('crearCuenta', {
    url: '/page12',
    templateUrl: 'templates/crearCuenta.html',
    controller: 'crearCuentaCtrl'
  })

  .state('tabsController.mTodoDePago', {
    url: '/page13',
    views: {
      'tab9': {
        templateUrl: 'templates/mTodoDePago.html',
        controller: 'mTodoDePagoCtrl'
      }
    }
  })

$urlRouterProvider.otherwise('/page11')


});